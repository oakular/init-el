(require 'treemacs)
(require 'treemacs-projectile)

(setq treemacs-icon-size 8)
(setq treemacs-width 35)

(provide 'init-treemacs)
