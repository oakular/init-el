(require 'magit)

(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)
(setq global-magit-file-mode t)

(setq git-commit-summary-max-length 50)
(setq git-commit-fill-column 72)
(setq git-commit-turn-on-auto-fill t)

(provide 'init-git)
;;; init-git.el ends here
