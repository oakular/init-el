(require 'csharp-mode)
(require 'omnisharp)

(setq omnisharp-server-executable-path "/usr/local/bin/omnisharp")

(add-hook 'csharp-mode-hook #'omnisharp-mode)
(add-hook 'csharp-mode-hook #'company-mode)
(add-hook 'csharp-mode-hook #'flycheck-mode)
(add-hook 'csharp-mode-hook #'smartparens-strict-mode)
(add-hook 'csharp-mode-hook #'highlight-indent-guides-mode)

(defun csharp-mode-setup ()
  (setq indent-tabs-mode nil)
  (setq c-syntactic-indentation t)
  (c-set-style "ellemtel")
  (setq c-basic-offset 4)
  (setq truncate-lines t)
  (setq tab-width 4)
  (setq evil-shift-width 4))

(add-hook 'csharp-mode-hook #'csharp-mode-setup)

(provide 'init-csharp)
