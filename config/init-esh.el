(require 'eshell)
(require 'eshell-toggle)

(setq eshell-buffer-name "esh")
(setq eshell-modify-global-environment t)
(setq eshell-prefer-lisp-functions t)
(setq eshell-rc-script "~/.eshell/profile")
(setq Man-notify-method 'bully)

(defun eshell-no-fringes ()
  (set-window-fringes nil 0 0)
  (set-window-margins nil 1 nil))
(add-hook 'eshell-mode-hook #'eshell-no-fringes)

(defun eshell-text-wrapping ()
  (visual-line-mode +1)
  (set-display-table-slot standard-display-table 0 ?\ ))
(add-hook 'eshell-mode-hook #'eshell-text-wrapping)

(add-hook 'eshell-mode-hook '(lambda () (define-key
                                          eshell-mode-map (kbd "C-l") 'eshell/clear)))

(require 'load-bash-alias)
(setq load-bash-alias-bashrc-file "~/.bashrc")

(global-set-key (kbd "C-`") 'eshell-toggle)

;; taken from doom-emacs
(defun +eshell--current-git-branch ()
  (let ((branch (car (cl-loop for match in (split-string (shell-command-to-string "git branch") "\n")
                              if (string-match-p "^\*" match)
                              collect match))))
    (if (not (eq branch nil))
        (format " (%s)" (substring branch 2))
      "")))

(setq eshell-toggle-use-projectile-root t)
(setq eshell-toggle-init-function #'shell)
(setq eshell-prompt-function (lambda nil
                               (concat
                                "λ " (user-login-name) "@" (system-name) " "
                                (if (string= (eshell/pwd) (getenv "HOME"))
                                    "~" (eshell/basename (eshell/pwd)))
                                (+eshell--current-git-branch)
                                (if (= (user-uid) 0) " # " "/ $ "))))
(provide 'init-esh)
