(require 'ivy-rich)

(add-hook 'emacs-startup-hook 'eshell)

(tool-bar-mode -1)
(scroll-bar-mode -1)
(setq use-file-dialog nil)
(setq use-dialog-box nil)
(fboundp 'toggle-frame-fullscreen)
;; Command-Option-f to toggle fullscreen mode
;; Hint: Customize `ns-use-native-fullscreen'
(global-set-key (kbd "M-ƒ") 'toggle-frame-fullscreen)
(setq initial-frame-alist '((top . 0) (left . 0) (width . 180) (height . 70)))

(load-theme 'doom-one t)
(solaire-global-mode 1)

(require 'doom-modeline)
(setq doom-modeline-height 15)
(doom-modeline-mode 1)

(set-face-attribute 'mode-line nil 
                    :box nil
                    :width 'condensed
                    :family "Verdana"
                    :height 110)

(set-face-attribute 'default nil :height 110)
(set-face-attribute 'default nil :width 'condensed)
(set-face-attribute 'default nil :family "Hack")

(add-hook 'display-line-numbers-mode-hook (lambda () (setq display-line-numbers 'relative)))

(global-set-key (kbd "M-h") 'ns-do-hide-emacs)
(global-set-key (kbd "M-˙") 'ns-do-hide-others)

(setq solarized-scale-org-headlines nil)
(setq solarized-high-contrast-mode-line t)
(setq solarized-use-variable-pitch t)

;; function to allow for alternating between horizontal and vertical splits
;; Source: https://www.emacswiki.org/emacs/ToggleWindowSplit
(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

(global-set-key (kbd "C-x |") 'toggle-window-split)

(setq calendar-location-name "Liverpool, UK") 
(setq calendar-latitude 53.36)
(setq calendar-longitude -2.91)

(ivy-rich-mode 1)

(setq eyebrowse-keymap-prefix "w")
(setq eyebrowse-mode-line-left-delimiter "WC: [")
(setq eyebrowse-wrap-around t)
(eyebrowse-mode 1)

(require 'highlight-indent-guides)
(setq highlight-indent-guides-method 'character)
;; (require 'theme-changer)
;; (change-theme 'solarized-light 'solarized-dark)

(provide 'init-frame)
