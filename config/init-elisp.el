(require 'company)

(add-hook 'emacs-lisp-mode-hook #'display-line-numbers-mode)

(add-hook 'emacs-lisp-mode-hook
          '(lambda ()
             (set (make-local-variable 'company-backends)
                  '((company-elisp) company-capf company-files)))
          )
(add-hook 'emacs-lisp-mode-hook #'company-mode)
(add-hook 'emacs-lisp-mode-hook #'flycheck-mode)
(add-hook 'emacs-lisp-mode-hook #'smartparens-strict-mode)
(add-hook 'emacs-lisp-mode-hook #'hl-line-mode)
(add-hook 'emacs-lisp-mode-hook #'git-gutter+-mode)

(provide 'init-elisp)

