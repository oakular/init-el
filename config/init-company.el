(require 'company)

(setq company-dabbrev-downcase nil)
(setq company-idle-delay 0)
(setq company-show-numbers t)

(eval-after-load
    'company
  '(add-to-list 'company-backends 'company-omnisharp))

(provide 'init-company)
