(require 'evil)
(require 'org)
(require 'org-evil)

;; (lexical-let ((default-color (cons (face-background 'mode-line)
;;                                    (face-foreground 'mode-line))))
;;   (add-hook 'post-command-hook
;;             (lambda ()
;;               (let ((color (cond ((minibufferp) default-color)
;;                                  ((evil-insert-state-p) '("#859900" . "#ffffff"))
;;                                  ((evil-emacs-state-p)  '("#6c71c4" . "#ffffff"))
;;                                  ((evil-normal-state-p)  '("#268bd2" . "#ffffff"))
;;                                  ((buffer-modified-p)   '("#006fa0" . "#ffffff"))
;;                                  (t default-color))))
;;                 (set-face-background 'mode-line (car color))
;;                 (set-face-foreground 'mode-line (cdr color))))))

(setq evil-search-wrap t
      evil-regexp-search t)

(evil-set-initial-state 'git-commit-mode 'insert)
(evil-set-initial-state 'evil-command-window-mode 'insert)
(evil-set-initial-state 'dired-mode 'emacs)
(evil-set-initial-state 'docker-container-mode 'emacs)
(evil-set-initial-state 'shell-mode 'emacs)
(evil-set-initial-state 'eshell-mode 'emacs)
(evil-set-initial-state 'term-mode 'emacs)
(evil-set-initial-state 'elfeed-search-mode 'emacs)
(evil-set-initial-state 'elfeed-show-mode 'emacs)

(evil-global-set-key 'normal ":" 'evil-command-window-ex)
(evil-global-set-key 'motion "j" 'evil-next-visual-line)
(evil-global-set-key 'motion "k" 'evil-previous-visual-line)

(add-hook 'org-insert-heading-hook #'evil-insert-state)
(add-hook 'org-insert-item-hook #'evil-insert-state)

(require 'evil-leader)
(global-evil-leader-mode)
(evil-leader/set-key "w" 'save-buffer)
(evil-leader/set-key "b" 'switch-to-buffer)
(evil-leader/set-key "g" 'keyboard-quit)

(evil-mode 1)

(provide 'init-evil)
