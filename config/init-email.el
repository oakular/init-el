(require 'org-mu4e)

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu/mu4e")
(require 'mu4e)

(setq mail-user-agent 'mu4e-user-agent)

(setq
 message-send-mail-function   'smtpmail-send-it
 smtpmail-default-smtp-server "smtp.office365.com"
 smtpmail-smtp-server         "smtp.office365.com"
 smtpmail-smtp-service        587
 smtpmail-local-domain        "office365.com")

(setq mu4e-get-mail-command "mbsync -Va")

(setq mu4e-maildir "/Users/callum/Maildir/personal")
(setq mu4e-trash-folder "/Trash")

(setq mu4e-show-images t)

(provide 'init-email)
